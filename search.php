<!DOCTYPE HTML>
<!--
	Editorial by Pixelarity
	pixelarity.com | hello@pixelarity.com
	License: pixelarity.com/license
-->
<html>
	<head>
		<title>Поиск | Clutter Box</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
		<!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
		<link rel="stylesheet" href="assets/css/main.css" />
		<!--[if lte IE 9]><link rel="stylesheet" href="assets/css/ie9.css" /><![endif]-->
		<!--[if lte IE 8]><link rel="stylesheet" href="assets/css/ie8.css" /><![endif]-->
	</head>
	<body>

		<!-- Wrapper -->
			<div id="wrapper">

				<!-- Main -->
					<div id="main">
						<div class="inner">

							<!-- Header -->
								<header id="header">
									<a href="index.html" class="logo"><strong>Clutter</strong> Box</a>
									<ul class="icons">
										<li><a href="#" class="icon fa-twitter"><span class="label">Twitter</span></a></li>
										<li><a href="#" class="icon fa-facebook"><span class="label">Facebook</span></a></li>
										<li><a href="#" class="icon fa-instagram"><span class="label">Instagram</span></a></li>
										<li><a href="#" class="icon fa-vk"><span class="label">Instagram</span></a></li>
									</ul>
								</header>

							<!-- Section -->
								<section>
									<header class="main">
										<h1>Результаты поиска по запросу «Антиквариат»</h1>
									</header>
									<div class="posts">
										<article>
											<a href="#" class="image"><img src="images/lot-1.jpg" alt="" /></a>
											<sup>Антиквариат</sup>
											<h3>Севрский фарфор</h3>
											<p>Французский фарфор, выпускаемый Севрским фарфоровым заводом, основанным в 1756 году близ Парижа.</p>
											<div class="lot">
												<div>
													<sub>Стартовая цена:</sub>
													<h2>39 000 ₽</h4>
												</div>
												<div>
													<code class="timer">16:54:12</code>
												</div>
											</div>
											<ul class="actions">
												<li><a href="#" class="button">Принять участие</a></li>
											</ul>
										</article>
										<article>
											<a href="#" class="image"><img src="images/lot-3.jpg" alt="" /></a>
											<sup>Антиквариат</sup>
											<h3>Серебряный подсвечник</h3>
											<p>Серебряный подсвечник "Олимпиада 1936".</p>
											<div class="lot">
												<div>
													<sub>Стартовая цена:</sub>
													<h2>39 000 ₽</h4>
												</div>
												<div>
													<code class="timer">16:54:12</code>
												</div>
											</div>
											<ul class="actions">
												<li><a href="#" class="button">Принять участие</a></li>
											</ul>
										</article>
									</div>
								</section>
								<hr>
								<ul class="pagination">
									<li><span class="button disabled">Раньше</span></li>
									<li><a href="#" class="page active">1</a></li>
									<li><a href="#" class="page">2</a></li>
									<li><a href="#" class="page">3</a></li>
									<li><span>&hellip;</span></li>
									<li><a href="#" class="page">8</a></li>
									<li><a href="#" class="page">9</a></li>
									<li><a href="#" class="page">10</a></li>
									<li><a href="#" class="button">Позже</a></li>
								</ul>

								<footer class="main-footer">
									<ul class="actions">
										<li><a class="button big no-border" href="index.html">Марки</a></li>
										<li class="active"><a class="button big no-border" href="generic.html">Бумажные деньги</a></li>
										<li><a class="button big no-border" href="elements.html">Антиквариат</a></li>
										<li><a class="button big no-border" href="#">Монеты</a></li>
										<li><a class="button big no-border" href="#">Разное</a></li>
									</ul>
									<ul class="icons">
										<li><a href="#" class="icon fa-twitter"><span class="label">Twitter</span></a></li>
										<li><a href="#" class="icon fa-facebook"><span class="label">Facebook</span></a></li>
										<li><a href="#" class="icon fa-instagram"><span class="label">Instagram</span></a></li>
										<li><a href="#" class="icon fa-vk"><span class="label">Instagram</span></a></li>
									</ul>
								</footer>

						</div>
					</div>

				<!-- Sidebar -->
					<div id="sidebar">
						<div class="inner">

							<!-- Search -->
								<section id="search" class="alt">
									<form method="post" action="#">
										<input type="text" name="query" id="query" placeholder="Поиск лота" />
									</form>
								</section>

							<!-- Menu -->
								<nav id="menu">
									<header class="major">
										<h2>Меню</h2>
									</header>
									<ul>
										<li><a href="index.html">Марки</a></li>
										<li><a href="generic.html">Бумажные деньги</a></li>
										<li><a href="elements.html">Антиквариат</a></li>
										<li><a href="#">Монеты</a></li>
										<li><a href="#">Разное</a></li>
									</ul>
								</nav>

								<div>
									<a href="#" class="button special big fit">Добавить лот</a>
									<a href="#" class="button fit no-border">Регистрация</a>
									<a href="#" class="button fit no-border">Вход</a>
								</div>



							<!-- Footer -->
								<footer id="footer">
									<p class="copyright">&copy; 2017-2018, Clutter Box. Интернет-аукцион</p>
									<p>Made by Artem Senin</p>
								</footer>

						</div>
					</div>

			</div>

		<!-- Scripts -->
			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/skel.min.js"></script>
			<script src="assets/js/util.js"></script>
			<!--[if lte IE 8]><script src="assets/js/ie/respond.min.js"></script><![endif]-->
			<script src="assets/js/main.js"></script>

	</body>
</html>
