<!DOCTYPE HTML>
<!--
	Editorial by Pixelarity
	pixelarity.com | hello@pixelarity.com
	License: pixelarity.com/license
-->
<html>
	<head>
		<title>Севрский фарфор | Clutter Box</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
		<!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
		<link rel="stylesheet" href="assets/css/main.css" />
		<!--[if lte IE 9]><link rel="stylesheet" href="assets/css/ie9.css" /><![endif]-->
		<!--[if lte IE 8]><link rel="stylesheet" href="assets/css/ie8.css" /><![endif]-->
	</head>
	<body>

		<!-- Wrapper -->
			<div id="wrapper">

				<!-- Main -->
					<div id="main">
						<div class="inner">

							<!-- Header -->
								<header id="header">
									<a href="index.html" class="logo"><strong>Clutter</strong> Box</a>
									<ul class="icons">
										<li><a href="#" class="icon fa-twitter"><span class="label">Twitter</span></a></li>
										<li><a href="#" class="icon fa-facebook"><span class="label">Facebook</span></a></li>
										<li><a href="#" class="icon fa-instagram"><span class="label">Instagram</span></a></li>
										<li><a href="#" class="icon fa-vk"><span class="label">Instagram</span></a></li>
									</ul>
								</header>

							<!-- Section -->
								<section class="single-lot">
									<header class="main">
										<h1>Мои ставки</h1>
									</header>
									<div class="row">
										<div class="12u$ 12u$(small)">

											<div class="table-wrapper">
												<table class="my-bet-list">

													<tbody>
														<tr class="bet--expires">
															<td class="bet-rates">
																<div class="bet-rates__img">
											            <img src="images/rate-1.jpg" width="54" height="40" alt="Сноуборд">
											          </div>
																<div>
																	<h4><a href="#">Севрский фарфор</a></h4>
																	<p>Телефон +7 900 667-84-48, звонить с 14 до 20</p>
																</div>
															</td>
															<td class="bet-category">
																Антиквариат
															</td>
															<td class="bet-timer">
																<div>
																	<code class="timer">16:54:12</code>
																</div>
															</td>
															<td class="bet-price">
																19 000 ₽
															</td>
															<td class="bet-time">5 минут назад</td>
														</tr>
														<tr class="bet--win">
															<td class="bet-rates">
																<div class="bet-rates__img">
											            <img src="images/rate-2.jpg" width="54" height="40" alt="Сноуборд">
											          </div>
																<div>
																	<h4><a href="#">Марки</a></h4>
																	<p>Телефон +7 900 667-84-48, звонить с 14 до 20</p>
																</div>
															</td>
															<td class="bet-category">
																Марки
															</td>
															<td class="bet-timer">
																<div>
																	<code class="timer">Ставка выиграна</code>
																</div>
															</td>
															<td class="bet-price">
																19 000 ₽
															</td>
															<td class="bet-time">5 минут назад</td>
														</tr>
														<tr class="bet--expired">
															<td class="bet-rates">
																<div class="bet-rates__img">
											            <img src="images/rate-2.jpg" width="54" height="40" alt="Сноуборд">
											          </div>
																<div>
																	<h4><a href="#">Марки</a></h4>
																</div>
															</td>
															<td class="bet-category">
																Марки
															</td>
															<td class="bet-timer">
																<div>
																	<code class="timer">Торги окончены</code>
																</div>
															</td>
															<td class="bet-price">
																19 000 ₽
															</td>
															<td class="bet-time">5 минут назад</td>
														</tr>
														<tr class="bet-expired">
															<td class="bet-rates">
																<div class="bet-rates__img">
											            <img src="images/rate-1.jpg" width="54" height="40" alt="Сноуборд">
											          </div>
																<div>
																	<h4><a href="#">Марки</a></h4>
																</div>
															</td>
															<td class="bet-category">
																Марки
															</td>
															<td class="bet-timer">
																<div>
																	<code class="timer">16:54:12</code>
																</div>
															</td>
															<td class="bet-price">
																19 000 ₽
															</td>
															<td class="bet-time">5 минут назад</td>
														</tr>
														<tr>
															<td class="bet-rates">
																<div class="bet-rates__img">
											            <img src="images/rate-3.jpg" width="54" height="40" alt="Сноуборд">
											          </div>
																<div>
																	<h4><a href="#">Марки</a></h4>
																</div>
															</td>
															<td class="bet-category">
																Марки
															</td>
															<td class="bet-timer">
																<div>
																	<code class="timer">16:54:12</code>
																</div>
															</td>
															<td class="bet-price">
																19 000 ₽
															</td>
															<td class="bet-time">5 минут назад</td>
														</tr>
												</table>

										</div>
									</div>

								</section>

								<footer class="main-footer">
									<ul class="actions">
										<li><a class="button big no-border" href="index.html">Марки</a></li>
										<li class="active"><a class="button big no-border" href="generic.html">Бумажные деньги</a></li>
										<li><a class="button big no-border" href="elements.html">Антиквариат</a></li>
										<li><a class="button big no-border" href="#">Монеты</a></li>
										<li><a class="button big no-border" href="#">Разное</a></li>
									</ul>
									<ul class="icons">
										<li><a href="#" class="icon fa-twitter"><span class="label">Twitter</span></a></li>
										<li><a href="#" class="icon fa-facebook"><span class="label">Facebook</span></a></li>
										<li><a href="#" class="icon fa-instagram"><span class="label">Instagram</span></a></li>
										<li><a href="#" class="icon fa-vk"><span class="label">Instagram</span></a></li>
									</ul>
								</footer>


						</div>
					</div>

				<!-- Sidebar -->
					<div id="sidebar">
						<div class="inner">

							<!-- Search -->
								<section id="search" class="alt">
									<form method="post" action="#">
										<input type="text" name="query" id="query" placeholder="Поиск лота" />
									</form>
								</section>

							<!-- Menu -->
								<nav id="menu">
									<header class="major">
										<h2>Меню</h2>
									</header>
									<ul>
										<li><a href="index.html">Марки</a></li>
										<li class="active"><a href="generic.html">Бумажные деньги</a></li>
										<li><a href="elements.html">Антиквариат</a></li>
										<li><a href="#">Монеты</a></li>
										<li><a href="#">Разное</a></li>
									</ul>
								</nav>

								<div>
									<a href="#" class="button special big fit">Добавить лот</a>
									<a href="#" class="button fit no-border">Регистрация</a>
									<a href="#" class="button fit no-border">Вход</a>
								</div>



							<!-- Footer -->
								<footer id="footer">
									<p class="copyright">&copy; 2017-2018, Clutter Box. Интернет-аукцион</p>
									<p>Made by Artem Senin</p>
								</footer>

						</div>
					</div>

			</div>

		<!-- Scripts -->
			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/skel.min.js"></script>
			<script src="assets/js/util.js"></script>
			<!--[if lte IE 8]><script src="assets/js/ie/respond.min.js"></script><![endif]-->
			<script src="assets/js/main.js"></script>

	</body>
</html>
