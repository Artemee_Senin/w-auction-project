<?php
define("ROOT_LOCATION", "/");

$is_auth = (bool) rand(0, 1);

$user_name = 'Константин';

// устанавливаем часовой пояс в Московское время
date_default_timezone_set('Europe/Moscow');

// записать в эту переменную оставшееся время в этом формате (ЧЧ:ММ)
//$lot_time_remaining = "48:00";

// временная метка для полночи следующего дня
$tomorrow = strtotime('tomorrow midnight');

// временная метка для настоящего времени
$now = strtotime('now');

// далее нужно вычислить оставшееся время до начала следующих суток и записать его в переменную $lot_time_remaining
$lot_time_remaining = gmdate("H:i:s", $tomorrow - $now);

$categories = [
    'antiques' => 'Антиквариат',
    'stamps' => 'Марки',
    'paper_money' => 'Бумажные деньги',
    'vintage' => 'Винтаж',
    'coins' => 'Монеты',
    'other' => 'Разное',
];
$products = [
  [
    'name' => 'Севрский фарфор',
		'description' => 'Французский фарфор, выпускаемый Севрским фарфоровым заводом, основанным в 1756 году близ Парижа.',
    'category' => $categories['antiques'],
    'price' => 10999,
    'url' => 'images/lot-1.jpg'
  ],
  [
    'name' => 'Марки 1930-1940 годы',
		'description' => 'Состояние люкс, полные серии.',
    'category' => $categories['stamps'],
    'price' => 159999,
    'url' => 'images/lot-2.jpg'],
  [
    'name' => 'Серебряный подсвечник',
		'description' => 'Серебряный подсвечник "Олимпиада 1936".',
    'category' => $categories['antiques'],
    'price' => 8000,
    'url' => 'images/lot-3.jpg'],
  [
    'name' => '5 рублей 1987-70 лет революции',
		'description' => 'Юбилейна монета СССР.',
    'category' => $categories['coins'],
    'price' => 10999,
    'url' => 'images/lot-4.jpg'],
  [
    'name' => 'Винтажное радио',
		'description' => 'Радио с подписью',
    'category' => $categories['vintage'],
    'price' => 7500,
    'url' => 'images/lot-5.jpg'],
  [
    'name' => 'Ламповый радиоприемник',
		'description' => 'Радио',
    'category' => $categories['vintage'],
    'price' => 5400,
    'url' => 'images/lot-6.jpg']
];
