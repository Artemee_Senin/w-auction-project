<?php
$get_category = $_GET['category'];
$categories = $template_args['category'];

if( isset($categories[$get_category]) ){
	$category = $categories[$get_category];
}
?>

<!-- Header -->
<header id="header">
		<a href="index.html" class="logo"><strong>Clutter</strong> Box</a>
		<ul class="icons">
			<li><a href="#" class="icon fa-twitter"><span class="label">Twitter</span></a></li>
			<li><a href="#" class="icon fa-facebook"><span class="label">Facebook</span></a></li>
			<li><a href="#" class="icon fa-instagram"><span class="label">Instagram</span></a></li>
			<li><a href="#" class="icon fa-vk"><span class="label">Instagram</span></a></li>
		</ul>
	</header>

<!-- Section -->
	<section>
		<header class="main">
			<?php if(isset($category)){
				echo "<h1>Все лоты в категории « $category »</h1>";
			}
			?>
		</header>
		<div class="posts">
			<?=include_template('./templates/category-lot.php', ['categories' => $template_args['categories'], 'products' => $template_args['products']]);?>
		</div>
	</section>
	<hr>
	<ul class="pagination">
		<li><span class="button disabled">Раньше</span></li>
		<li><a href="#" class="page active">1</a></li>
		<li><a href="#" class="page">2</a></li>
		<li><a href="#" class="page">3</a></li>
		<li><span>&hellip;</span></li>
		<li><a href="#" class="page">8</a></li>
		<li><a href="#" class="page">9</a></li>
		<li><a href="#" class="page">10</a></li>
		<li><a href="#" class="button">Позже</a></li>
	</ul>

	<footer class="main-footer">
		<ul class="actions">
			<li><a class="button big no-border" href="index.html">Марки</a></li>
			<li class="active"><a class="button big no-border" href="generic.html">Бумажные деньги</a></li>
			<li><a class="button big no-border" href="elements.html">Антиквариат</a></li>
			<li><a class="button big no-border" href="#">Монеты</a></li>
			<li><a class="button big no-border" href="#">Разное</a></li>
		</ul>
		<ul class="icons">
			<li><a href="#" class="icon fa-twitter"><span class="label">Twitter</span></a></li>
			<li><a href="#" class="icon fa-facebook"><span class="label">Facebook</span></a></li>
			<li><a href="#" class="icon fa-instagram"><span class="label">Instagram</span></a></li>
			<li><a href="#" class="icon fa-vk"><span class="label">Instagram</span></a></li>
		</ul>
	</footer>