<?php
  $products = $template_args['products'];
  $categories = $template_args['categories'];

  if( isset($categories[$_GET['category']]) ):
    $category = $_GET['category'];

    foreach($products as $index => $product):
      if( $product['category'] == $categories[$category] ):
      ?>
        <article>
          <a href="#" class="image"><img src="<?=$product['url'];?>" alt="" /></a>
          <sup><?=$product['category'];?></sup>
          <h3><?=$product['name'];?></h3>
          <!--p>Французский фарфор, выпускаемый Севрским фарфоровым заводом, основанным в 1756 году близ Парижа.</p-->
          <div class="lot">
            <div>
              <sub>Стартовая цена:</sub>
              <h2><?=$product['price'];?> ₽</h4>
            </div>
            <div>
              <code class="timer">16:54:12</code>
            </div>
          </div>
          <ul class="actions">
            <li><a href="#" class="button">Принять участие</a></li>
          </ul>
        </article>
      <?php
      endif;
    endforeach;
  else:
    echo '<h1>Сожалеем, такой категории товара не существует</h1>';
    http_response_code(404);
  endif;
  