<?php

// устанавливаем часовой пояс в Московское время
date_default_timezone_set('Europe/Moscow');

// ставки пользователей, которыми надо заполнить таблицу
$bets = [
		['name' => 'Иван', 'price' => 11500, 'ts' => strtotime('-' . rand(1, 50) .' minute')],
		['name' => 'Константин', 'price' => 11000, 'ts' => strtotime('-' . rand(1, 18) .' hour')],
		['name' => 'Евгений', 'price' => 10500, 'ts' => strtotime('-' . rand(25, 50) .' hour')],
		['name' => 'Семён', 'price' => 10000, 'ts' => strtotime('last week')]
];


$item_id = $_GET['id'];
?>


							<!-- Header -->
								<header id="header">
									<a href="index.html" class="logo"><strong>Clutter</strong> Box</a>
									<ul class="icons">
										<li><a href="#" class="icon fa-twitter"><span class="label">Twitter</span></a></li>
										<li><a href="#" class="icon fa-facebook"><span class="label">Facebook</span></a></li>
										<li><a href="#" class="icon fa-instagram"><span class="label">Instagram</span></a></li>
										<li><a href="#" class="icon fa-vk"><span class="label">Instagram</span></a></li>
									</ul>
								</header>

							<!-- Section -->
								<section class="single-lot">
									<?php
									if(isset($template_args['products'][$item_id])):
									$lot_data = $template_args['products'][$item_id];
									?>
									<header class="main">
										<h1><?=$lot_data['name'];?></h1>
									</header>
									<div class="row">
											<div class="8u 12u$(small)">
												<span class="image fit"><img src="<?=$lot_data['url'];?>" alt=""></span>
												<p><sup><b>Категория: </b><?=$lot_data['category'];?></sup></p>
												<p><?=$lot_data['description'];?></p>
											</div>
											<div class="4u$ 12u$(small)">
												<div class="box">
													<div>
														<code class="timer">16:54:12</code>
													</div>
													<div class="lot">
														<div>
															<sub>Текущая цена:</sub>
															<h2><?=$lot_data['price'];?> ₽</h4>
														</div>
														<div>
															<p>Мин. ставка: <?=$lot_data['price']+1000;?> ₽</p>
														</div>
													</div>

													<form method="post" action="#">

															<label for="bet">Ваша ставка:</label>
														<div class="row uniform">
															<div class="6u 12u$(xsmall)">
																<input type="number" name="bet" id="bet" value="" placeholder="0" />
															</div>
															<div class="6u 12u$(xsmall)">
																<button type="submit" class="button special">Сделать ставку</button>
															</div>
														</div>
													</form>
												</div>

												<h4>История ставок (10)</h4>
												<div class="table-wrapper">
													<table>
														<thead>
															<tr>
																<th>Имя</th>
																<th>Сумма</th>
																<th>Время</th>
															</tr>
														</thead>
														<tbody>
															<?php for($i = 0; $i < count($bets); $i++):
                         				$current_bet = $bets[$i]; ?>
																<tr>
																	<td><?=$current_bet['name']?></td>
																	<td><?=$current_bet['price']?> ₽</td>
																	<td><?=time_converter($current_bet['ts']);?></td>
																</tr>
															<?php endfor;?>
													</table>
												</div>

											</div>
										</div>

										<?php
											else:
										?>
										<header class="main">
											<h1>Сожалеем, такого товара не существует</h1>
										</header>
										<?php
							      http_response_code(404);
							     	endif;
										?>


								</section>
								<footer class="main-footer">
									<ul class="actions">
										<li><a class="button big no-border" href="index.html">Марки</a></li>
										<li class="active"><a class="button big no-border" href="generic.html">Бумажные деньги</a></li>
										<li><a class="button big no-border" href="elements.html">Антиквариат</a></li>
										<li><a class="button big no-border" href="#">Монеты</a></li>
										<li><a class="button big no-border" href="#">Разное</a></li>
									</ul>
									<ul class="icons">
										<li><a href="#" class="icon fa-twitter"><span class="label">Twitter</span></a></li>
										<li><a href="#" class="icon fa-facebook"><span class="label">Facebook</span></a></li>
										<li><a href="#" class="icon fa-instagram"><span class="label">Instagram</span></a></li>
										<li><a href="#" class="icon fa-vk"><span class="label">Instagram</span></a></li>
									</ul>
								</footer>
