<?php
$categories = $template_args['categories'];
?>
<!DOCTYPE HTML>
<!--
	Editorial by Pixelarity
	pixelarity.com | hello@pixelarity.com
	License: pixelarity.com/license
-->
<html>
	<head>
		<title><?=$template_args['page_title']?> | Clutter Box</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
		<!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
		<link rel="stylesheet" href="assets/css/main.css" />
		<!--[if lte IE 9]><link rel="stylesheet" href="assets/css/ie9.css" /><![endif]-->
		<!--[if lte IE 8]><link rel="stylesheet" href="assets/css/ie8.css" /><![endif]-->
	</head>
	<body>

		<!-- Wrapper -->
			<div id="wrapper">

				<!-- Main -->
					<div id="main">
						<div class="inner">
	            <?=$template_args['content'];?>
						</div>
					</div>

				<!-- Sidebar -->
					<div id="sidebar">
						<div class="inner">

							<!-- Search -->
								<section id="search" class="alt">
									<form method="post" action="#">
										<input type="text" name="query" id="query" placeholder="Поиск лота" />
									</form>
								</section>

							<!-- Menu -->
								<nav id="menu">
									<header class="major">
										<h2>Меню</h2>
									</header>
									<ul>
										<?php foreach($categories as $key => $value):?>
											<li><a href="index.html"><?=$value?></a></li>
										<?php endforeach;?>
									</ul>
								</nav>

								<div class="user-menu">
									<?php
										if($is_auth):
									?>
										<a href="#" class="button special big fit">Добавить лот</a>
										<p class="logged-as">Вы вошли как:</p>
										<h2>Константин</h2>
										<a href="login.html">Выйти</a>
									<?php
										else:
									?>
										<a href="#" class="button fit no-border">Вход</a>
										<a href="#" class="button fit no-border">Регистрация</a>
									<?php endif; ?>

								</div>



							<!-- Footer -->
								<footer id="footer">
									<p class="copyright">&copy; 2017-2018, Clutter Box. Интернет-аукцион</p>
									<p>Made by Artem Senin</p>
								</footer>

						</div>
					</div>

			</div>

		<!-- Scripts -->
			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/skel.min.js"></script>
			<script src="assets/js/util.js"></script>
			<!--[if lte IE 8]><script src="assets/js/ie/respond.min.js"></script><![endif]-->
			<script src="assets/js/main.js"></script>

	</body>
</html>
