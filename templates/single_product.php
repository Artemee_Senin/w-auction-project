<?php
  $item = $template_args['item'];
  $properties = $template_args['properties'];
  $lot_time_remaining = $template_args['lot_time_remaining'];
?>
<article>
  <a href="<?=ROOT_LOCATION . 'lot.php?id=' . $item?>" class="image"><img src="<?=$properties['url']?>" alt="" /></a>
  <sup><?=$properties['category']?></sup>
  <h3><?=$properties['name']?></h3>
  <p><?=$properties['description']?></p>
  <div class="lot">
    <div>
      <sub>Стартовая цена:</sub>
      <h2><?=$properties['price']?> ₽</h2>
    </div>
    <div>
      <code class="timer"><?=$lot_time_remaining;?></code>
    </div>
  </div>
  <ul class="actions">
    <li><a href="#" class="button">Принять участие</a></li>
  </ul>
</article>
