<?php
$categories = $template_args['categories'];
$products = $template_args['products'];
$lot_time_remaining = $template_args['lot_time_remaining'];
?>


  <!-- Header -->
    <header id="header">
      <a href="index.html" class="logo"><strong>Clutter</strong> Box</a>
      <ul class="icons">
        <li><a href="#" class="icon fa-twitter"><span class="label">Twitter</span></a></li>
        <li><a href="#" class="icon fa-facebook"><span class="label">Facebook</span></a></li>
        <li><a href="#" class="icon fa-instagram"><span class="label">Instagram</span></a></li>
        <li><a href="#" class="icon fa-vk"><span class="label">Instagram</span></a></li>
      </ul>
    </header>

  <!-- Banner -->
    <section id="banner">
      <div class="content">
        <header>
          <h1>Нужно пополнить коллекцию?</h1>
          <p>На нашем интернет-аукционе вы найдете самый ламповый экземпляр</p>
        </header>
        <p>Вы можете выставить на аукцион свои предметы или выкупить другие</p>
      </div>
      <span class="image object">
        <img src="images/promo-bg.jpg" alt="" />
      </span>
    </section>

  <!-- Section -->
    <section>
      <header class="major">
        <h2>Категории</h2>
      </header>
      <div class="features">
        <?php foreach($categories as $key => $value):?>
        <a class="link" href="#">
          <span class="icon fa-picture-o"></span>
          <div class="content">
            <h3><?=$value?></h3>
            <!--p>Филателия – область коллекционирования и изучения знаков почтовой оплаты.</p-->
          </div>
        </a>
        <?php endforeach;?>
      </div>
    </section>

  <!-- Section -->
    <section>
      <header class="major">
        <h2>Открытые лоты</h2>
      </header>
      <div class="posts">
        <?php foreach($products as $item => $properties):?>
         <?=include_template('./templates/single_product.php', ['item' => $item, 'properties' => $properties, 'lot_time_remaining' => $template_args['lot_time_remaining']]);?>
       <?php endforeach;?>
      </div>
    </section>

    <footer class="main-footer">
      <ul class="actions">
        <?php foreach($categories as $key => $value):?>
          <li class="<?php echo $key == 'stamps' ? 'active' : ''?>"><a class="button big no-border" href="index.html"><?=$value?></a></li>
        <?php endforeach;?>
      </ul>
      <ul class="icons">
        <li><a href="#" class="icon fa-twitter"><span class="label">Twitter</span></a></li>
        <li><a href="#" class="icon fa-facebook"><span class="label">Facebook</span></a></li>
        <li><a href="#" class="icon fa-instagram"><span class="label">Instagram</span></a></li>
        <li><a href="#" class="icon fa-vk"><span class="label">Instagram</span></a></li>
      </ul>
    </footer>
