<!DOCTYPE HTML>
<!--
	Editorial by Pixelarity
	pixelarity.com | hello@pixelarity.com
	License: pixelarity.com/license
-->
<html>
	<head>
		<title>Регистрация | Clutter Box</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
		<!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
		<link rel="stylesheet" href="assets/css/main.css" />
		<!--[if lte IE 9]><link rel="stylesheet" href="assets/css/ie9.css" /><![endif]-->
		<!--[if lte IE 8]><link rel="stylesheet" href="assets/css/ie8.css" /><![endif]-->
	</head>
	<body>

		<!-- Wrapper -->
			<div id="wrapper">

				<!-- Main -->
					<div id="main">
						<div class="inner">

							<!-- Header -->
								<header id="header">
									<a href="index.html" class="logo"><strong>Clutter</strong> Box</a>
									<ul class="icons">
										<li><a href="#" class="icon fa-twitter"><span class="label">Twitter</span></a></li>
										<li><a href="#" class="icon fa-facebook"><span class="label">Facebook</span></a></li>
										<li><a href="#" class="icon fa-instagram"><span class="label">Instagram</span></a></li>
										<li><a href="#" class="icon fa-vk"><span class="label">Instagram</span></a></li>
									</ul>
								</header>

							<!-- Section -->
								<section>
									<header class="main">
										<h1>Регистрация нового аккаунта</h1>
									</header>
									<div class="row">
											<div class="6u 12u$(small)">
											<form method="post" action="#">
																<div class="row uniform">
																	<div class="12u 12u$(xsmall)">
																		<label for="name">Ваше имя</label>
																		<input type="text" name="name" id="name" value="" placeholder="Имя">
																	</div>
																	<div class="12u$ 12u$(xsmall)">
																		<label for="email">E-mail</label>
																		<input type="email" name="email" id="email" value="" placeholder="Email">
																	</div>
																	<div class="12u$ 12u$(xsmall)">
																		<label for="password">Пароль</label>
																		<input type="password" name="password" id="password" value="" placeholder="Email">
																	</div>
																	<div class="12u$">
																		<label for="message">Контактные данные</label>
																		<textarea name="message" id="message" placeholder="Как с вами связаться" rows="6"></textarea>
																	</div>

																	<!-- Break -->
																	<div class="12u$">
																		<ul class="actions">
																			<li><input type="submit" value="Зарегистрироваться" class="special"></li>
																			<li><a class="button no-border" href="#">Уже есть аккаунт</a></li>
																		</ul>
																	</div>
																</div>
															</form>
														</div>
								</section>

								<footer class="main-footer">
									<ul class="actions">
										<li><a class="button big no-border" href="index.html">Марки</a></li>
										<li class="active"><a class="button big no-border" href="generic.html">Бумажные деньги</a></li>
										<li><a class="button big no-border" href="elements.html">Антиквариат</a></li>
										<li><a class="button big no-border" href="#">Монеты</a></li>
										<li><a class="button big no-border" href="#">Разное</a></li>
									</ul>
									<ul class="icons">
										<li><a href="#" class="icon fa-twitter"><span class="label">Twitter</span></a></li>
										<li><a href="#" class="icon fa-facebook"><span class="label">Facebook</span></a></li>
										<li><a href="#" class="icon fa-instagram"><span class="label">Instagram</span></a></li>
										<li><a href="#" class="icon fa-vk"><span class="label">Instagram</span></a></li>
									</ul>
								</footer>


				<!-- Sidebar -->
					<div id="sidebar">
						<div class="inner">

							<!-- Search -->
								<section id="search" class="alt">
									<form method="post" action="#">
										<input type="text" name="query" id="query" placeholder="Поиск лота" />
									</form>
								</section>

							<!-- Menu -->
								<nav id="menu">
									<header class="major">
										<h2>Меню</h2>
									</header>
									<ul>
										<li><a href="index.html">Марки</a></li>
										<li><a href="generic.html">Бумажные деньги</a></li>
										<li><a href="elements.html">Антиквариат</a></li>
										<li><a href="#">Монеты</a></li>
										<li><a href="#">Разное</a></li>
									</ul>
								</nav>

								<div class="user-menu">
									<a href="#" class="button special big fit">Добавить лот</a>
									<a href="#" class="button fit no-border">Регистрация</a>
									<a href="#" class="button fit no-border">Вход</a>
									<!--p class="logged-as">Вы вошли как:</p>
									<h2>Константин</h2>
									<a href="login.html">Выйти</a-->
								</div>



							<!-- Footer -->
								<footer id="footer">
									<p class="copyright">&copy; 2017-2018, Clutter Box. Интернет-аукцион</p>
									<p>Made by Artem Senin</p>
								</footer>

						</div>
					</div>

			</div>

		<!-- Scripts -->
			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/skel.min.js"></script>
			<script src="assets/js/util.js"></script>
			<!--[if lte IE 8]><script src="assets/js/ie/respond.min.js"></script><![endif]-->
			<script src="assets/js/main.js"></script>

	</body>
</html>
