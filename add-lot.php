<!DOCTYPE HTML>
<!--
	Editorial by Pixelarity
	pixelarity.com | hello@pixelarity.com
	License: pixelarity.com/license
-->
<html>
	<head>
		<title>Добавление лота | Clutter Box</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
		<!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
		<link rel="stylesheet" href="assets/css/main.css" />
		<!--[if lte IE 9]><link rel="stylesheet" href="assets/css/ie9.css" /><![endif]-->
		<!--[if lte IE 8]><link rel="stylesheet" href="assets/css/ie8.css" /><![endif]-->
	</head>
	<body>

		<!-- Wrapper -->
			<div id="wrapper">

				<!-- Main -->
					<div id="main">
						<div class="inner">

							<!-- Header -->
								<header id="header">
									<a href="index.html" class="logo"><strong>Clutter</strong> Box</a>
									<ul class="icons">
										<li><a href="#" class="icon fa-twitter"><span class="label">Twitter</span></a></li>
										<li><a href="#" class="icon fa-facebook"><span class="label">Facebook</span></a></li>
										<li><a href="#" class="icon fa-instagram"><span class="label">Instagram</span></a></li>
										<li><a href="#" class="icon fa-vk"><span class="label">Instagram</span></a></li>
									</ul>
								</header>

							<!-- Section -->
								<section>
									<header class="main">
										<h1>Добавление лота</h1>
									</header>
									<div class="row">
											<div class="6u 12u$(small)">
											<form method="post" action="#" class="form form--add-lot container form--invalid">
																<div class="row uniform">

																	<div class="6u 12u$(xsmall)">
																		<label for="lotname">Наименование</label>
																		<input type="text" name="lotname" id="lotname" value="" placeholder="Наименование">
																		<span class="form__error">Введите наименование лота</span>
																	</div>
																	<div class="6u 12u$(xsmall) form__item--invalid">
																		<label for="category">Категория</label>
																		<div class="select-wrapper">
																			<select name="category" id="category">
																				<option value="">- Category -</option>
																				<option value="">Марки</option>
																				<option value="">Бумажные деньги</option>
																				<option value="">Антиквариат</option>
																				<option value="">Монеты</option>
																				<option value="">Разное</option>
																			</select>
																		</div>
																		<span class="form__error">Выберите категорию лота</span>
																	</div>
																	<div class="12u$ form__item--invalid">
																		<label for="description">Описание</label>
																		<textarea name="description" id="description" placeholder="Описание лота" rows="6"></textarea>
																		<span class="form__error">Введите описание лота</span>
																	</div>

																	<div class="12u$ form__item form__item--file"> <!-- form__item--uploaded -->
															      <label>Изображение</label>
															      <div class="preview">
															        <button class="preview__remove" type="button">x</button>
															        <div class="preview__img">
															          <img src="img/avatar.jpg" width="113" height="113" alt="Изображение лота">
															        </div>
															      </div>
															      <div class="form__input-file">
															        <input class="visually-hidden" type="file" id="photo2" value="" style="display: block; height: 100px;">
															        <label for="photo2">
															          <span>+ Добавить</span>
															        </label>
															      </div>
															    </div>

																	<div class="3u 6u$(xsmall)">
																		<label for="price">Начальная цена</label>
																		<input type="number" name="price" id="price" value="" placeholder="0" />
																		<span class="form__error">Введите начальную цену лота</span>
																	</div>
																	<div class="3u 6u$(xsmall)">
																		<label for="bet">Шаг ставки</label>
																		<input type="number" name="bet" id="bet" value="" placeholder="0" />
																		<span class="form__error">Введите шаг ставки лота</span>
																	</div>
																	<div class="6u 12u$(xsmall)">
																		<label for="date">Дата окончания торгов</label>
																		<input type="date" name="date" id="date" value=""/>
																		<span class="form__error">Введите корректную дату окончания лота</span>
																	</div>

																	<div class="12u$">
																		<span class="form__error form__error--bottom">Пожалуйста, исправьте ошибки в форме.</span>
																	</div>

																	<!-- Break -->
																	<div class="12u$">
																		<ul class="actions">
																			<li><input type="submit" value="Добавить лот" class="special"></li>
																		</ul>
																	</div>
																</div>
															</form>
														</div>
														</div>


								</section>
								<footer class="main-footer">
									<ul class="actions">
										<li><a class="button big no-border" href="index.html">Марки</a></li>
										<li class="active"><a class="button big no-border" href="generic.html">Бумажные деньги</a></li>
										<li><a class="button big no-border" href="elements.html">Антиквариат</a></li>
										<li><a class="button big no-border" href="#">Монеты</a></li>
										<li><a class="button big no-border" href="#">Разное</a></li>
									</ul>
									<ul class="icons">
										<li><a href="#" class="icon fa-twitter"><span class="label">Twitter</span></a></li>
										<li><a href="#" class="icon fa-facebook"><span class="label">Facebook</span></a></li>
										<li><a href="#" class="icon fa-instagram"><span class="label">Instagram</span></a></li>
										<li><a href="#" class="icon fa-vk"><span class="label">Instagram</span></a></li>
									</ul>
								</footer>

				<!-- Sidebar -->
					<div id="sidebar">
						<div class="inner">

							<!-- Search -->
								<section id="search" class="alt">
									<form method="post" action="#">
										<input type="text" name="query" id="query" placeholder="Поиск лота" />
									</form>
								</section>

							<!-- Menu -->
								<nav id="menu">
									<header class="major">
										<h2>Меню</h2>
									</header>
									<ul>
										<li><a href="index.html">Марки</a></li>
										<li><a href="generic.html">Бумажные деньги</a></li>
										<li><a href="elements.html">Антиквариат</a></li>
										<li><a href="#">Монеты</a></li>
										<li><a href="#">Разное</a></li>
									</ul>
								</nav>

								<div class="user-menu">
									<a href="#" class="button special big fit">Добавить лот</a>
									<a href="#" class="button fit no-border">Регистрация</a>
									<a href="#" class="button fit no-border">Вход</a>
									<!--p class="logged-as">Вы вошли как:</p>
									<h2>Константин</h2>
									<a href="login.html">Выйти</a-->
								</div>



							<!-- Footer -->
								<footer id="footer">
									<p class="copyright">&copy; 2017-2018, Clutter Box. Интернет-аукцион</p>
									<p>Made by Artem Senin</p>
								</footer>

						</div>
					</div>


			</div>
			</div>
			</div>

		<!-- Scripts -->
			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/skel.min.js"></script>
			<script src="assets/js/util.js"></script>
			<!--[if lte IE 8]><script src="assets/js/ie/respond.min.js"></script><![endif]-->
			<script src="assets/js/main.js"></script>

	</body>
</html>
